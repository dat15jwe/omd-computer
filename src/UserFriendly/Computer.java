package UserFriendly;

import UnderTheHood.Factorial;
import UnderTheHood.Program;

/**
 * Created by Wilmer on 2016-09-17.
 */

public class Computer {

    public Memory memory;
    public Program program;
    private int counter;

    public Computer(Memory memory) {
        this.memory = memory;
        counter = 0;
    }

    public void load(Program program) {
        this.program = program;

    }

    public void run() {
        while (counter != -1) {
            counter = this.program.get(counter).execute(memory, counter);
        }
    }


    public static void main(String[] args) {
        Program factorial = new Factorial();
        System.out.println(factorial);
        Computer computer = new Computer(new LongMemory(1024));
        computer.load(factorial);
        computer.run();
    }
}