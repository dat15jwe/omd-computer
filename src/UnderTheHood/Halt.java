package UnderTheHood;

import UserFriendly.Memory;

/**
 * Created by Wilmer on 2016-09-18.
 */
public class Halt implements Instruction{

    @Override
    public int execute(Memory memory, int counter) {
        return -1;
    }
    public String toString(){
        return "HLT"+"\n";
    }
}
