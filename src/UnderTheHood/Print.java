package UnderTheHood;

import UserFriendly.Memory;

/**
 * Created by Wilmer on 2016-09-18.
 */
public class Print implements Instruction{

    public Operator operator;

    public Print(Operator operator){
        this.operator = operator;
    }

    @Override
    public int execute(Memory memory, int counter) {
        System.out.println(operator.getWord(memory));
        return ++counter;
    }
    public String toString(){
        return "PRT "+String.valueOf(operator)+"\n";
    }
}
