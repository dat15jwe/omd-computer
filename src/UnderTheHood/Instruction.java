package UnderTheHood;

import UserFriendly.Memory;

/**
 * Created by Wilmer on 2016-09-17.
 */
public interface Instruction {

    int execute(Memory memory, int counter);

}


