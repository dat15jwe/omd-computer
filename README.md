Inlämningsuppgift 1 — computer

1. Vi har
public class Computer
  public interface Memory
    public class LongMemory implements Memory
    public class VeryLongMemory implements Memory //extra
  public interface Word
    public class LongWord
    public class VeryLongWord // extra
  public abstract class Program
      private class Address implements instruction
      private class Copy implements instruction
      private class JumpEq implements instruction
        abstract class AritmOpInstruction
        private class Mul implements instruction
        private class Add implements instruction
      private class Jump implements instruction
      private class Print implements instruction
      private class Halt implements instruction
    public class Factorial extends Program
    }



2. Klassen Program består av en rad instruktioner. I nuläget används en metod
add() för att vad och i vilken ordning som ska exekveras under programmets gång.
Denna metod måste implementeras antingen i Program klassen själv eller i klasser
som implementeras av Program. För att undvika detta kan man låta klassen Program
utvidga/expand en List-klass från Java.Util(Javas standardbibliotek). Detta
gör att klassen Program redan har en intern lista samt att metoden add() redan
finns implementerad från List.
Public class Program extends List/ArrayList/Linkedlist/Stack<"Insert classname here">{


3. Klassen Computer är något användaren behöver kunna se. För att kunna skriva program behöver man tillgång till klasserna som implementerar Instruction och det blir snabbt mycket backend. Därav bör allt annat placeras i ett eget paket.


4. Klassen program bör implementera ett Commandinterface. På så sätt kommer varje nytt program endast behöva köra en "Do();"


5. klasserna mul och add kan extend:a en potentiell
      abtract class AritmOpInstruction{
        calc(a,b,adress);
      }


6. Om program implementerar <Instruction> som har metoden calc(); och alla operatorer också implementerar detta.


7. Instruktionen exekveras i Addklassen som extendar en AritmOpklass. AritmOp är superklass till både Add och Mul.


Byt ut List<E> till mer konkret

Pil till lista

skriv på linjen till <<instruction>> "ArrayList"


Add:
op1 - kan va word eller address: gör interface operator med metod getWord(); strategy
op2
res

ingen do() i program

counter i computer. Jumpinstruktionen byter countervärde.


-uppdaterat klassdiagram
-kod
